namespace arvoreBinaria
{
    public class no
    {
        public int valor;
        public no Esquerda;
        public no Direita;

        public no(int valor)
        {
            this.valor = valor;
            this.Esquerda = null;
            this.Direita = null;

        }
        
    }
}