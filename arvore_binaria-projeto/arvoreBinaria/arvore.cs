using System;
namespace arvoreBinaria
{
    public class arvore
    {
        public readonly no noRaiz;
        
        public void inserir(int valor)
        {
            no novoNo = new no(valor);
            no cursorAntigo = null;
            no cursor = this.noRaiz;
            novoNo.valor = valor;
            while (cursor != null)
            {
                if (novoNo.valor < cursor.valor)
                {
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                if (cursor != null && novoNo.valor >= cursor.valor)
                {
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                }
                
            }

            if (valor < cursorAntigo.valor)
            {
                cursorAntigo.Esquerda = novoNo;
            }

            if (valor >= cursorAntigo.valor)
            {
                cursorAntigo.Direita = novoNo;
            
            }
            
        }

        public int remover(int valor)
        {
            no removido = buscar(valor);
            
            if (removido.valor == -1)
            {
                return removido.valor;
            }
            
            
            no cursorAntigo = null;
            no cursor = this.noRaiz;
            
            // cursor chegar no nó 'removido'
            while (cursor.valor != removido.valor)
            {
                if (valor < cursor.valor)
                {
                    //caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                if (valor > cursor.valor)
                {
                    // caminha para a Direita
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                }
            }
            
            //Se for raiz
            if (noRaiz == cursor)
            {
                if (cursor.Esquerda == null && cursor.Direita == null)
                {
                    cursor = null;
                }

                //Se existe nó à esquerda
                if (cursor.Esquerda != null)
                {
                    // caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;

                    while (cursor.Direita != null)
                    {
                        // caminha para a Direita
                        cursorAntigo = cursor;
                        cursor = cursor.Direita;
                    }

                    no ramo = cursor;

                    while (cursor.Esquerda != null)
                    {
                        // caminha para a Esquerda
                        cursorAntigo = cursor;
                        cursor = cursor.Esquerda;
                    }

                    no cursorAntigoRemover = removido;
                    no cursorRemover = removido;
                    cursorRemover = cursorRemover.Esquerda;
                    while (cursorRemover != ramo)
                    {
                        cursorAntigoRemover = cursorRemover;
                        cursorRemover = cursorRemover.Direita;
                    }

                    cursorAntigoRemover.Direita = null;

                    cursor.Esquerda = removido.Esquerda;

                    // cursor chegar no nó 'removido'
                    cursorAntigo = this.noRaiz;
                    cursor = this.noRaiz;
                    while (cursor.valor != removido.valor)
                    {
                        if (valor < cursor.valor)
                        {
                            //caminha para a Esquerda
                            cursorAntigo = cursor;
                            cursor = cursor.Esquerda;
                        }

                        if (valor > cursor.valor)
                        {
                            // caminha para a Direita
                            cursorAntigo = cursor;
                            cursor = cursor.Direita;
                        }
                    }
                    //
                    
                    cursorAntigo = null;
                    cursor = this.noRaiz;
                    cursorAntigo.Direita = ramo;
                }
            }
            
            //Se for folha só remove
            if (cursor.Esquerda == null && cursor.Direita == null)
            {
                if (cursorAntigo.Esquerda == cursor)
                {
                    cursorAntigo.Esquerda = null;
                }

                if (cursorAntigo.Direita == cursor)
                {
                    cursorAntigo.Direita = null;
                }
                removido = cursor;
                return removido.valor;
            }

            //Se não existe nó à esquerda transforma o nó 'removido' no nó à sua direita
            if (cursor.Esquerda == null)
            {
                if (cursorAntigo.Esquerda == cursor)
                {
                    cursor = cursor.Direita;
                    cursorAntigo.Esquerda = cursor;
                }

                if (cursorAntigo.Direita == cursor)
                {
                    cursor = cursor.Direita;
                    cursorAntigo.Direita = cursor;
                }

                
                return removido.valor;
            }
            
            // //Se existe uma folha à esquerda
            // if (cursor.Esquerda != null && cursor.Esquerda.Esquerda == null && cursor.Esquerda.Direita == null)
            // {
            //     // caminha para a Esquerda
            //     cursorAntigo = cursor;
            //     cursor = cursor.Esquerda;
            //
            //     no ramo = cursor;
            //     
            //     // cursor chegar no nó 'removido'
            //     cursorAntigo = null;
            //     cursor = this.noRaiz;
            //     while (cursor.valor != removido.valor)
            //     {
            //         if (valor < cursor.valor)
            //         {
            //             //caminha para a Esquerda
            //             cursorAntigo = cursor;
            //             cursor = cursor.Esquerda;
            //         }
            //
            //         if (valor > cursor.valor)
            //         {
            //             // caminha para a Direita
            //             cursorAntigo = cursor;
            //             cursor = cursor.Direita;
            //         }
            //     }
            //     //
            //
            //     ramo.Direita = cursor.Direita;
            //     cursorAntigo.Esquerda = ramo;
            //
            //     return removido.valor;
            // }
            
            //Se existe nó à Direita
            if (cursor.Direita != null)
            {
                // caminha para a Direita
                cursorAntigo = cursor;
                cursor = cursor.Direita;
                
                while (cursor.Esquerda != null)
                {
                    // caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                no ramo = cursor;
                
                // while (cursor.Direita != null)
                // {
                //     // caminha para a Direita
                //     cursorAntigo = cursor;
                //     cursor = cursor.Direita;
                // }
                
                no cursorAntigoRemover = removido;
                no cursorRemover = removido;
                cursorRemover = cursorRemover.Direita;
                while (cursorRemover != ramo)
                {
                    cursorAntigoRemover = cursorRemover;
                    cursorRemover = cursorRemover.Esquerda;
                }

                cursorAntigoRemover.Esquerda = null;
                
                cursor.Direita = removido.Direita;

                // cursor chegar no nó 'removido'
                cursorAntigo = this.noRaiz;
                cursor = this.noRaiz;
                while (cursor.valor != removido.valor)
                {
                    if (valor < cursor.valor)
                    {
                        //caminha para a Esquerda
                        cursorAntigo = cursor;
                        cursor = cursor.Esquerda;
                    }

                    if (valor > cursor.valor)
                    {
                        // caminha para a Direita
                        cursorAntigo = cursor;
                        cursor = cursor.Direita;
                    }
                }
                //

                cursorAntigo = ramo;

                return removido.valor;
            }
            
            
            return 0;
        }

        public no buscar(int valor)
        {
            
            no cursorAntigo = null;
            no cursor = this.noRaiz;
            
            while (cursor != null)
            {
                if (valor == cursor.valor)
                {
                    return cursor;
                }
                if (valor < cursor.valor)
                {
                    //caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                if (valor > cursor.valor)
                {
                    // caminha para a Direita
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                }
                
            }

            return new no(-1);

        }

        public arvore(int valor)
        {
            noRaiz = new no(valor);
        }
    }
    
}